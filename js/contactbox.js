/*** contactbox ***/
function cb() {
        jQuery('#contact_box').height(jQuery('#contact_box').find('.whitebg').height());
        setTimeout(function () {
                
                jQuery('#contact_box').css('top', -parseInt(jQuery('#contact_box').height())); // +3px for shadow
                jQuery('#contact_box .whitebg').css('visible', 'hidden');
                var arrow = jQuery('.contact_tab a span:last-child');
                if (arrow.hasClass('icon-arrow-up')) {
                        arrow.removeClass('icon-arrow-up');
                        arrow.addClass('icon-arrow-down');
                }
        }, 400);
}

function toggle_cb() {
        jQuery('.contact_tab').on('click', function () {
                if (jQuery(this).parent().parent().find('#contact_box').css('top') != '0px') {
                        jQuery(this).parent().parent().find('#contact_box').css('top', '0');
                        jQuery('#contact_box .whitebg').css('visible', 'visible');
                } else {
                        jQuery(this).parent().parent().find('#contact_box').css('top', jQuery('#contact_box').css('top', -parseInt(jQuery('#contact_box').height() + 3)));
                        jQuery('#contact_box .whitebg').css('visible', 'hidden');
                }
        });
        jQuery('#contact_box .icon-cancel').on('click', function () {
                jQuery(this).parent().parent().find('#contact_box').css('top', jQuery('#contact_box').css('top', -parseInt(jQuery('#contact_box').height() + 3)));
                jQuery('#contact_box .whitebg').css('visible', 'hidden');
        });
        jQuery('#cframe').contents(jQuery("select")).bind("change", function () {
                jQuery("#cframe").height(jQuery("#cframe").contents().find("html").height());
        });
}

function cb_arrow() {
        jQuery('.contact_tab a').on('click', function () {
                var arrow = jQuery(this).children('span:last-child');
                if (arrow.hasClass('icon-arrow-down')) {
                        arrow.removeClass('icon-arrow-down');
                        arrow.addClass('icon-arrow-up');
                } else {
                        arrow.removeClass('icon-arrow-up');
                        arrow.addClass('icon-arrow-down');
                }
        });
        jQuery('#contact_box .icon-cancel').on('click', function () {
                var arrow = jQuery('.contact_tab a span:last-child');
                if (arrow.hasClass('icon-arrow-down')) {
                        arrow.removeClass('icon-arrow-down');
                        arrow.addClass('icon-arrow-up');
                } else {
                        arrow.removeClass('icon-arrow-up');
                        arrow.addClass('icon-arrow-down');
                }
        });
}


jQuery(document).ready(function () {
        jQuery(window).on('load', cb);
        toggle_cb();
        cb_arrow();
});


//var ajaxURLContactboxGetContacts = 'http://www.publisso.de/open-access-publizieren/buecher-publizieren/?type=1419337801&tx_dreipccontact_contactbox%5Baction%5D=getContacts&tx_dreipccontact_contactbox%5Bcontroller%5D=Contact';
//var ajaxURLContactboxGetContacts = 'http://publisso.rheinware.eu/GetContactBoxData';
var ajaxURLContactboxGetContacts = '/system/getContact';

jQuery(document).ready(function () {
        // contact-box-top
        jQuery('.tx_dreipc_contact_contact_box_top select.choose-concern').change(function () {
                ajaxPositionsHandler(jQuery(this));
        });
        
        jQuery('#topbox-positions-ajax-container').on('change', '.select-positions', function (e) {
                e.preventDefault();
                return ajaxContactsHandler(jQuery(this));
        });
        
});


function ajaxPositionsHandler(el) {
        var depId = jQuery(el).val();
        
        jQuery('#topbox-positions-ajax-container').empty();
        jQuery('#topbox-contacts-ajax-container').empty();
        
        jQuery.post(ajaxURLContactboxGetPositions, {'tx_dreipccontact_contactbox[category]': depId}, function (json) {
                jQuery("#topbox-positions-ajax-container").html(json.content).show();
        }, "json");
        
        return false;
}

function ajaxContactsHandler(el) {
        jQuery('#topbox-contacts-ajax-container').empty();
        var posId = jQuery(el).val();
        jQuery.post(ajaxURLContactboxGetContacts, {'tx_dreipccontact_contactbox[position]': posId}, function (json) {
                jQuery("#topbox-contacts-ajax-container").html(json.content).show();
        }, "json");
        
        return false;
        
}
