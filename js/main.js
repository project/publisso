(function (jQuery, Drupal) {
        
                Drupal.behaviors.publissoGoldBehavior = {
                        attach: function (context, settings) {
                                
                                var sliderPartners = jQuery('.partnerslider .slick');
                                sliderPartners.slick({
                                        dots: true,
                                        slidesToShow: 1
                                });
                        
                                // contact panel toggle
                                (function contactToggle() {
                                        var contactPanel = jQuery('#contact-panel');
                                        var toggler = jQuery('.toggler');
                                
                                        jQuery('.toggler, .panel-close').on('click', function (event) {
                                                event.preventDefault();
                                                if (toggler.hasClass('on')) {
                                                        toggler.removeClass('on').prev('.panel__inner').removeClass('slideDown').addClass('slideUp');
                                                } else {
                                                        toggler.addClass('on').prev('.panel__inner').removeClass('slideUp').addClass('slideDown');
                                                }
                                        });
                                })();
                        
                                // Dropdown menu keyboard navigation
                        
                                jQuery('header .menu').accessibleDropDownMenu();
                        
                                // menu mobile
                                (function menuMobile() {
                                        var window = jQuery(window);
                                        var winWidth = jQuery(window).width();
                                        // mobile menu open
                                        var toggler = jQuery('#mobile-togger'),
                                                menu = jQuery('#main-menu');
                                
                                        toggler.on('click', function () {
                                                if (jQuery(this).hasClass('icon-cancel')) {
                                                        jQuery(this).removeClass('icon-cancel').addClass('icon-menu');
                                                        jQuery('body').removeClass('overlay');
                                                } else {
                                                        jQuery(this).removeClass('icon-menu').addClass('icon-cancel');
                                                        jQuery('body').addClass('overlay');
                                                }
                                        });
                                        // remove overlay if resizing with the open mobile menu
                                        // checks if window width has changed or it's just a mobile browser triggered an event. !important
                                        window.on('resize', function () {
                                                if (window.width() >= 768 & window.width() != winWidth) {
                                                        toggler.removeClass('icon-cancel').addClass('icon-menu');
                                                        jQuery('.navbar-collapse').removeClass('in');
                                                        jQuery('body').removeClass('overlay');
                                                }
                                        });
                                })();
                        
                                // tab autocollapse
                                var autocollapse = function () {
                                
                                        // tab container
                                        var tabs = jQuery('#tabs'),
                                                tab = tabs.children('li'),
                                                container = jQuery('#collapsed');
                                        //height of the container. Used to detect when tabs are starting to wrap
                                        var tabsHeight = tabs.innerHeight(),
                                                tabHeight = tab.outerHeight();
                                
                                        if (container.length > 0 && tabsHeight >= (tabHeight + 10)) {
                                        
                                                while (tabsHeight > (tabHeight + 10)) {
                                                
                                                        var children = tabs.children('li:not(:last-child)');
                                                        var count = children.length;
                                                        jQuery(children[count - 1]).prependTo(jQuery('#collapsed'));
                                                        tabsHeight = tabs.innerHeight();
                                                }
                                        } else {
                                        
                                                if (container.length > 0 && tabsHeight <= tabHeight) {
                                                
                                                        var collapsed = jQuery('#collapsed').children('li');
                                                        var count = collapsed.length;
                                                        jQuery(collapsed[0]).insertBefore(tabs.children('li:last-child'));
                                                        tabsHeight = tabs.innerHeight();
                                                }
                                                // check if no wrapping
                                                if (tabsHeight >= (tabHeight + 10)) {
                                                        autocollapse();
                                                }
                                        }
                                };
                                autocollapse();
                                
                                //.off().on() - unbind events before bind to prevent fireing events multiple times
                                
                                //var offcanvas_toggled = false;
                                jQuery(window).off('resize').on('resize', autocollapse());
                        
                                // offcanvas panel
                                (function offcanvasPanel() {
                                        jQuery('.offcanvas-toggle, .offcanvas-close').off('click').on('click', function (event) {
                                                
                                                event.preventDefault();
                                                jQuery('.panel-container').toggleClass('open');
                                                jQuery('body').toggleClass('overlay');
                                        });
                                
                                        jQuery("#offcanvas-panel .dropdown-toggle").off('click').on("click", function (e) {
                                        
                                                var icon = jQuery(this).find('span');
                                                var current = jQuery(this).siblings('.sub-menu');
                                                var grandparent = jQuery(this).parent().parent();
                                                if (icon.hasClass('icon-arrow-up') || icon.hasClass('icon-arrow-down'))
                                                        icon.toggleClass('icon-arrow-down icon-arrow-up');
                                                current.toggle().parent().toggleClass('on');
                                                e.stopPropagation();
                                        });
                                })();

                                (function toggleTOCArrow(jQuery){
                                        jQuery('nav#offcanvas-panel span#icon').on('click', function(){
                                                jQuery(this).toggleClass('icon-arrow-down icon-arrow-up');
                                        });
                                })(jQuery);
                        }
                };



                (jQuery('nav#offcanvas-panel div[data-toggle="collapse"]').on('click', function(){
                        (jQuery(this).parent().find('span#icon').toggleClass('icon-arrow-up icon-arrow-down'));
                }));


})(jQuery, Drupal);


// -----------------Dropdown menu keyboard navigation ------------------//
/* @author    Beau Charman | @beaucharman | http://www.beaucharman.github.io */
(function (jQuery) {
        "use strict";
        jQuery.fn.accessibleDropDownMenu = function (options) {
                var settings = jQuery.extend({
                        'item': 'li',
                        'anchor': 'a'
                }, options);
                
                // menu instance
                var menu = jQuery(this);
                
                // accessible via :focus
                jQuery(settings.anchor, menu).focus(function () {
                        jQuery(this).parents(settings.item).addClass('focus');
                }).blur(function () {
                        jQuery(this).parents(settings.item).removeClass('focus');
                });
        };
})(jQuery);

